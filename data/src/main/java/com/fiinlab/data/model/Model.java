package com.fiinlab.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

public class Model extends RealmObject {

    @PrimaryKey
    @Required
    @SerializedName("imei")
    @Expose
    private String imei;

    @Required
    @SerializedName("latitude")
    @Expose
    private String latitude;

    @Required
    @SerializedName("longitude")
    @Expose
    private String longitude;

    @Required
    @SerializedName("app_name")
    @Expose
    private String appName;

    @Required
    @SerializedName("version_name")
    @Expose
    private String versioName;

    @Required
    @SerializedName("version_code")
    @Expose
    private String versionCode;

    @Required
    @SerializedName("dn")
    @Expose
    private String dn;

    @Required
    @SerializedName("status_http")
    @Expose
    private String statusHTTP;

    @Required
    @SerializedName("description")
    @Expose
    private String description;

    @Required
    @SerializedName("date_time")
    @Expose
    private String dateTime;

    @Required
    @SerializedName("request")
    @Expose
    private String request;

    @Required
    @SerializedName("response")
    @Expose
    private String response;


    @Required
    @SerializedName("time_phone")
    @Expose
    private String timePhone;

    @Required
    @SerializedName("android_version")
    @Expose
    private String androidVersion;


    @Required
    @SerializedName("battery_charge")
    @Expose
    private String battery;

    @Required
    @SerializedName("carrier_name")
    @Expose
    private String carrier;


    @Required
    @SerializedName("device_name")
    @Expose
    private String deviceName;


    @Required
    @SerializedName("url")
    @Expose
    private String url;


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getVersioName() {
        return versioName;
    }

    public void setVersioName(String versioName) {
        this.versioName = versioName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public String getDn() {
        return dn;
    }

    public void setDn(String dn) {
        this.dn = dn;
    }

    public String getStatusHTTP() {
        return statusHTTP;
    }

    public void setStatusHTTP(String statusHTTP) {
        this.statusHTTP = statusHTTP;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    public String getTimePhone() {
        return timePhone;
    }

    public void setTimePhone(String timePhone) {
        this.timePhone = timePhone;
    }

    public String getAndroidVersion() {
        return androidVersion;
    }

    public void setAndroidVersion(String androidVersion) {
        this.androidVersion = androidVersion;
    }

    public String getBattery() {
        return battery;
    }

    public void setBattery(String battery) {
        this.battery = battery;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }


    @Override
    public String toString() {
        return "Model{" +
                "imei='" + imei + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", appName='" + appName + '\'' +
                ", versioName='" + versioName + '\'' +
                ", versionCode='" + versionCode + '\'' +
                ", dn='" + dn + '\'' +
                ", statusHTTP='" + statusHTTP + '\'' +
                ", description='" + description + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", request='" + request + '\'' +
                ", response='" + response + '\'' +
                ", timePhone='" + timePhone + '\'' +
                ", androidVersion='" + androidVersion + '\'' +
                ", battery='" + battery + '\'' +
                ", carrier='" + carrier + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
