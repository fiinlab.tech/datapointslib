package com.fiinlab.data.dataservice;

import java.io.IOException;

import okhttp3.Response;

public interface IDataServiceCallback {
    void failure(IOException e);

    void notSuccessful(int code, String message);

    void success(Response response);
}
