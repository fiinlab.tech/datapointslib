package com.fiinlab.data.dataservice;


import com.fiinlab.data.model.Model;

public interface IDataService {
    void sendData(IDataServiceCallback callback, Model model, String URL);
}
