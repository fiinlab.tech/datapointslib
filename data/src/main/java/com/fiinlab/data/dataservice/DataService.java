package com.fiinlab.data.dataservice;

import android.util.Log;


import com.fiinlab.data.model.Model;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class DataService implements IDataService, Callback {
    private IDataServiceCallback mCallback;
    private final MediaType json = MediaType.parse("application/json; charset=utf-8");


    @Override
    public void sendData(IDataServiceCallback callback, Model model, String URL) {
        this.mCallback = callback;

        Type type = new TypeToken<Model>() {}.getType();
        String json3 = new Gson().toJson(model, type);

        RequestBody body = RequestBody.create(json, json3);
        Request request = new Request.Builder()
                .url(URL)
                .post(body)
                .build();


        Log.d("Services", json3);

        OkHttpClient client = new OkHttpClient();
        client.newCall(request).enqueue(this);
    }

    @Override
    public void onFailure(@androidx.annotation.NonNull Call call, @androidx.annotation.NonNull IOException e) {
        this.mCallback.failure(e);
    }

    @Override
    public void onResponse(@androidx.annotation.NonNull Call call, @androidx.annotation.NonNull Response response) {
        if (!response.isSuccessful()) {
            this.mCallback.notSuccessful(response.code(), response.message());
        } else {
            this.mCallback.success(response);
        }
    }

}
