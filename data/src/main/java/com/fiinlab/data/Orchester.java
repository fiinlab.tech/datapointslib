package com.fiinlab.data;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.location.Location;
import android.util.Log;

import androidx.core.app.ActivityCompat;


import com.fiinlab.data.dataservice.DataService;
import com.fiinlab.data.dataservice.IDataService;
import com.fiinlab.data.dataservice.IDataServiceCallback;
import com.fiinlab.data.model.Model;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.Response;

public class Orchester extends
        DataApplication implements IDataServiceCallback {

    private Activity activity;
    private IDataService mLoginService;

    public Orchester(Activity activity) {
        this.activity = activity;
        mLoginService = new DataService();
    }

    public void sendData(final String endpoint, String description, String request, String response, String statusHTTP, String dn, String url) {
        Log.d("Services", endpoint);
        Log.d("Services", description);
        Log.d("Services", request);
        Log.d("Services", response);
        Log.d("Services", statusHTTP);
        Log.d("Services", dn);
        Log.d("Services", url);


        String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());
        String currentTime = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date());

        String imei = Obtencion.returnImei(activity);


        final Model model = new Model();
        model.setRequest(Obtencion.encode(request));
        model.setResponse(Obtencion.encode(response));
        model.setTimePhone(currentDate + " " + currentTime);
        model.setStatusHTTP(statusHTTP);
        model.setDn(dn);
        model.setDescription(description);
        model.setDateTime("");
        model.setUrl(url);
        model.setAndroidVersion(Obtencion.getAndroidVersion());
        model.setDeviceName(Obtencion.getDeviceName());
        model.setAppName(Obtencion.returnAppName(activity));
        model.setVersioName(Obtencion.returnVersionName(activity));
        model.setVersionCode(String.valueOf(Obtencion.returnVersionCode(activity)));
        model.setImei(imei);
        model.setCarrier(Obtencion.getCarrierName(activity));
        model.setBattery(String.valueOf(Obtencion.getBatteryCharge(activity)));


        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);

        if ((activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)
                && (activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(activity, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION}, 0);
        } else {

            fusedLocationClient.getLastLocation().addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {

                        model.setLongitude(String.valueOf(location.getLatitude()));
                        model.setLatitude(String.valueOf(location.getLongitude()));

                        mLoginService.sendData(Orchester.this, model, endpoint);
                    }
                }
            });
        }
    }

    @Override
    public void failure(IOException e) {
        Log.d("Data", e.getMessage() + "");
    }

    @Override
    public void notSuccessful(int code, String message) {
        Log.d("Data", code + " " + message);
    }

    @Override
    public void success(Response response) {
        try {
            Log.d("Data", response.body().string());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
