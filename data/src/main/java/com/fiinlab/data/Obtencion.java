package com.fiinlab.data;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.BatteryManager;
import android.os.Build;

import android.os.Bundle;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import androidx.core.app.ActivityCompat;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Base64;
import java.util.List;


public class Obtencion {

    private static final int PERMISSIONS_REQUEST_READ_PHONE_STATE = 0;

    public Obtencion(Activity activity) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(activity);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "");
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        /*
        DataApplication application = (DataApplication) activity.getApplication();
        Tracker mTracker = application.getDefaultTracker();
        Log.i("Data", "Setting screen name: " + "Obtencion");
        mTracker.setScreenName("Image~" + "Obtencion");

        //
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

        //Build and send event.
        mTracker.send(new HitBuilders.EventBuilder()
                .setCategory("Action")
                .setAction("Share")
                .build());

        // Build and send timing.
        mTracker.send(new HitBuilders.TimingBuilder()
                .setCategory("Timer")
                .setValue(0)
                .setVariable("Name")
                .setLabel("Label")
                .build());*/

    }


    public static String returnImei(Activity activity) {
        String imei = "";
        TelephonyManager telephonyManager = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        if ((activity.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED)) {

            ActivityCompat.requestPermissions(activity, new String[]{
                    Manifest.permission.READ_PHONE_STATE}, PERMISSIONS_REQUEST_READ_PHONE_STATE);
        } else {
            imei = telephonyManager.getDeviceId();
        }

        return imei;
    }


    public static String returnVersionName(Activity activity) {
        String versionName = "";
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            versionName = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionName;
    }


    public static int returnVersionCode(Activity activity) {
        int versionCode = 0;
        try {
            PackageInfo pInfo = activity.getPackageManager().getPackageInfo(activity.getPackageName(), 0);
            versionCode = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return versionCode;
    }


    public static String returnAppName(Activity activity) {
        ApplicationInfo applicationInfo = activity.getApplicationInfo();
        int stringId = applicationInfo.labelRes;
        return stringId == 0 ? applicationInfo.nonLocalizedLabel.toString() : activity.getString(stringId);
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.toLowerCase().startsWith(manufacturer.toLowerCase())) {
            return model.toUpperCase();
        } else {
            return manufacturer + " " + model.toUpperCase();
        }
    }

    public static String getAndroidVersion() {
        String release = Build.VERSION.RELEASE;
        int sdkVersion = Build.VERSION.SDK_INT;
        return "Android SDK: " + sdkVersion + " (" + release + ")";
    }


    public static int getBatteryCharge(Activity activity) {
        IntentFilter batIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent battery = activity.registerReceiver(null, batIntentFilter);

        return battery.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
    }

    public static String getCarrierName(Activity activity) {
        TelephonyManager telephonyManager = ((TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE));
        return telephonyManager.getNetworkOperatorName();
    }


    public static void getSIMinfo(Activity activity) {
        //above 22
        if (Build.VERSION.SDK_INT > 23) {
            //for dual sim mobile
            SubscriptionManager localSubscriptionManager = SubscriptionManager.from(activity);

            if (activity.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            if (localSubscriptionManager.getActiveSubscriptionInfoCount() > 1) {
                //if there are two sims in dual sim mobile
                List localList = localSubscriptionManager.getActiveSubscriptionInfoList();
                SubscriptionInfo simInfo = (SubscriptionInfo) localList.get(0);
                SubscriptionInfo simInfo1 = (SubscriptionInfo) localList.get(1);

                final String sim1 = simInfo.getDisplayName().toString();
                final String sim2 = simInfo1.getDisplayName().toString();

            } else {
                //if there is 1 sim in dual sim mobile
                TelephonyManager tManager = (TelephonyManager) activity.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
                String sim1 = tManager.getNetworkOperatorName();
            }

        } else {
            //below android version 22
            TelephonyManager tManager = (TelephonyManager) activity.getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
            String sim1 = tManager.getNetworkOperatorName();
        }
    }


    public static String encode(String str){
        byte[] bytesEncoded = Base64.getEncoder().encode(str.getBytes());
        return new String(bytesEncoded);
    }


}


