package com.fiinlab.datapoints;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.fiinlab.data.Orchester;

public class MainActivity extends AppCompatActivity {
    private Orchester orchester;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        orchester = new Orchester(this);


        orchester.sendData(
                "http://compartfon.work.fiinlab-tech.com:8080/api/datapoints/data/collector",
                "Example",
                "request",
                "response",
                "200",
                "1234567890",
                "url");
    }
}
