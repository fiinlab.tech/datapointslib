## DataPoints Fiinlab



### Implementación


En el archivo root build.gradle
```
allprojects {
		repositories {
		//Agrega el repositorio de jitpack
			maven { url 'https://jitpack.io' }
			credentials { username authToken }

		}
}
```


Se agrega la depencencia al archivo app/build.gradle
```
dependencies {
			//Implementa la librería
	        implementation 'mx.labsfiinlab.developers.git.compartfon:datapointslib:-SNAPSHOT'
}
```


Se agrega en el archivo gradle.properties


```
authToken=jp_rb3lid8ub112u31ipk14a036qe
```

Ejemplo de uso en código JAVA para envio de datos al MS de DataCenter

    private Orchester orchester = new Orchester(activity);
	
	//Ejemplo:
     orchester.sendData(Constants.ENDPOINT_VERIFICACION, "LoginService", json,
	 response.body().string(),
               "", String.valueOf(response.code()), "", Constants.ENDPOINT_LOGIN);
			   
	 //Nombre de  los campos en orden de envio
	sendData(endpoint, description, request, response, timeServer, statusHTTP, dn, url)

   
    


- Donde el primer campo es la URL o ENDPOINT es el lugar a donde se mandara la info en formato String

- El segundo campo es la descripción del evento o log que se esta haciendo por SERVICE

- El tercer campo es el request de la petición 

- El cuarto es el response del SERVICE de la misma petición

- El quinto campo es el time del server que debe ir vacío

- El sexto es el status HTTP de la petición  

- El septimo el DN del usuario logeado (Si es que es un aplicativo cliente, de lo contrario debe ir vacío)

- El octavo debe tener la URL o ENDPOINT del servicio o microservicio a donde va la petición 
